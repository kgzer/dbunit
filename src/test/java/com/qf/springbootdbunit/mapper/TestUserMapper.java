package com.qf.springbootdbunit.mapper;

import com.qf.springbootdbunit.SpringbootDbunitApplication;
import com.qf.springbootdbunit.pojo.User;
import com.qf.springbootdbunit.utils.AbstractDbunitTestCase;
import org.dbunit.DatabaseUnitException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

@RunWith(SpringRunner.class)//下面的注解是SpringBoot对dbunit的支持
@SpringBootTest(classes = SpringbootDbunitApplication.class)//和原来测试的区别(导入了SpringBoot的运行环境)
public class TestUserMapper extends AbstractDbunitTestCase {
    private User exUser;//期望返回的数据
    @Autowired
    private DataSource dataSource;

    @Autowired
    private UserMapper userMapper;

    public TestUserMapper() throws DatabaseUnitException {
        super("testdata.xml");
    }


    @Before                   //前置通知
    public void init() throws SQLException, DatabaseUnitException, IOException {
        exUser=new User(1,"xiaoming","123456");
        setConn(dataSource.getConnection());
        backOneTable("nz_user");
        insertTestData();
    }


    @Test
    public void testFindUserById(){
        //调用方法
        User acUser = userMapper.findUserById(1);
        //断言(就是看请求回来的数据 和预期的数据是否一致)
        Assert.assertEquals(exUser.getId(),acUser.getId());
        Assert.assertEquals(exUser.getUserName(),acUser.getUserName());
        Assert.assertEquals(exUser.getPassword(),acUser.getPassword());
    }


    /**
     * 还原数据
     */
    @After
    public void destory() throws FileNotFoundException, DatabaseUnitException, SQLException {
        resumeTable();
    }
}
