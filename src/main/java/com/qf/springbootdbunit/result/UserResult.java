package com.qf.springbootdbunit.result;

import com.qf.springbootdbunit.pojo.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Auther: xiaobobo
 * @Date: 2020/2/18 11:22
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResult extends BaseResult{
    private User user;
    private List<User> users;
}
