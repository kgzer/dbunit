package com.qf.springbootdbunit.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: xiaobobo
 * @Date: 2020/2/18 11:21
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseResult {
    //每一次给前端返回的状态
    private int state;   // 0:请求失败   1：请求成功
    private String errorMsg;  //错误的原因是什么
}
