package com.qf.springbootdbunit.mapper;

import com.qf.springbootdbunit.pojo.User;

public interface UserMapper {
    /**
     * 通过id找到用户
     * @param id
     * @return
     */
    User findUserById(int id);
}
