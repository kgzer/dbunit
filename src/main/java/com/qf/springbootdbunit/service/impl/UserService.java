package com.qf.springbootdbunit.service.impl;

import com.qf.springbootdbunit.mapper.UserMapper;
import com.qf.springbootdbunit.pojo.User;
import com.qf.springbootdbunit.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Auther: xiaobobo
 * @Date: 2020/2/18 11:16
 * @Description:用户的业务逻辑类
 */
@Service
@Transactional
public class UserService implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User findUserById(int id) {
        return userMapper.findUserById(id);
    }
}
