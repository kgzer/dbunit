package com.qf.springbootdbunit.service;

import com.qf.springbootdbunit.pojo.User;

/**
 * @Auther: xiaobobo
 * @Date: 2020/2/18 11:15
 * @Description:
 */
public interface IUserService {
    /**
     * 通过id找用户
     * @param id
     * @return
     */
    User findUserById(int id)throws Exception;
}
