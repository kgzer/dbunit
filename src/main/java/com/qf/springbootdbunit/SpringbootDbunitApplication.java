package com.qf.springbootdbunit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDbunitApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootDbunitApplication.class, args);
    }

}
