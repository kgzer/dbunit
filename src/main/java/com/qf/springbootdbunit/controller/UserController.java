package com.qf.springbootdbunit.controller;

import com.qf.springbootdbunit.pojo.User;
import com.qf.springbootdbunit.result.UserResult;
import com.qf.springbootdbunit.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private IUserService userService;

    /**
     * 通过id找用用户
     * @param id
     * @return
     */
    @RequestMapping("findUserById")
    public UserResult findUserById(int id){
        UserResult result = new UserResult();
        result.setState(0);   //默认是失败的
        try {
            User user = userService.findUserById(id);
            result.setState(1);
            result.setUser(user);
        } catch (Exception e) {
            //说明请求是失败的
            result.setErrorMsg("通过id获取用户数据失败:"+e.getMessage());
        }
        return result;
    }

}
