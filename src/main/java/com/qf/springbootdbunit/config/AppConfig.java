package com.qf.springbootdbunit.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Auther: xiaobobo
 * @Date: 2020/2/18 11:07
 * @Description:
 */
@SpringBootConfiguration     //表示是一个配置文件
@ComponentScan(basePackages = {"com.qf.springbootdbunit"}) //这个就是原来spring配置中的扫描
@MapperScan(basePackages = {"com.qf.springbootdbunit.mapper"})   //这个是扫描  mapper接口所在的位置
public class AppConfig {

}
